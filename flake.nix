# TODO:
# - a nix run option to continuously:
#   - regenerate project (with project-generator)
#   - build haskell library and copy it into the right place
# - [bonus] a nix build option to export Godot project
# - [bonus] a nix run option for running project through editor
# - [bonus] a nix run option for running exported project
{
  description = "A basic godot-haskell project flake";

  inputs.godot-haskell.url = "gitlab:lboklin/godot-haskell";

  outputs = { self, nixpkgs, godot-haskell }:
    let outputs-for = system:
      let hs-lib-name = "godot-haskell-template";

          pkgs = import nixpkgs {
            inherit system;
            overlays = [ godot-haskell.overlay ];
          };

          hs-lib = pkgs.haskellPackages.callCabal2nix hs-lib-name ./. {};

      in {
        packages."${system}"."${hs-lib-name}" = hs-lib;

        defaultPackage."${system}" = hs-lib;

        apps."${system}" = {
          inherit (godot-haskell.apps."${system}") project-generator;
        };

        devShell."${system}" = pkgs.mkShell {
          buildInputs = with hs-lib.getBuildInputs;
            let ghc = pkgs.haskellPackages.ghcWithHoogle (_: haskellBuildInputs);
            in
              [ # pkgs.godot # good if you're on NixOS -- otherwise you'll face issues with gfx drivers
                pkgs.haskell-language-server
                pkgs.haskellPackages.cabal-install
                ghc
              ] ++
              propagatedBuildInputs ++
              systemBuildInputs;
        };
      };

    in outputs-for "x86_64-linux";
}
