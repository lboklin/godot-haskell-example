The official Godot demo from the manual: Dodge the Creeps using godot-haskell bindings and a purely nix-based workflow. Requires Nix 3 with nix flakes feature enabled and Godot 3.3.3.

### Basic workflow

To enter the development environment, run `nix develop`. It should include what you need except for Godot itself (see `flake.nix`) and an editor (+ its LSP integration). From this environment you can launch your editor and work on the code. Assuming your editor has LSP integration set up correctly, you should have working auto-complete. You can also directly launch the editor with `nix develop -c $MY_EDITOR $SRC_FILE $ETC`.

To keep your Haskell and Godot projects in sync, leave the project generator running while working: `nix run .#project-generator game src` (or `nix develop -c godot-haskell-project-generator game src`).

Build with `nix build` then copy the lib file from `result/` over into `./game/lib/`,  e.g. `cp -f result/lib/ghc-8.10.7/libmyproject.so ./game/lib/`.

Every build is kept in the nix store, so it's recommended to garbage collect sometimes, for example by running `nix store delete --ignore-liveness $(readlink result)` before rebuilding, or with this fancier long-liner every time you build: `SPATH=$(readlink result) nix build && nix store delete $SPATH &>/dev/null || echo "Nothing to garbage collect"`. A build script (`./build.sh`) is provided for reference.

Load up the game by importing `game/project.godot` into the editor, or launch the editor with the project loaded directly with `godot --path game -e`. To run the game in the editor press F5, stop it with F8.


### Understanding the structure

There are two parts to every project. `game` which is the Godot project and `src` which are the Haskell sources. When you run `nix build`, it builds the Haskell project (using cabal under the hood) and any dependent libraries as necessary. The build result is added to the `/nix/store/` with a symlink in the local directory to it called `result`. In there you will find the compiled library file which needs to be copied into `game/lib/`. This way Godot will pick it up. If you do not copy this file, Godot will continue to use the old compiled version (if any).

You must regenerate `src/Project` any time you modify the Godot project. This directory contains the Godot project mirrored into Haskell, just like @Servant@ provides you with API safety by declaring APIs in Haskell. When you change the name of a node in Godot, this will update a Haskell class instance, which will lead to a type error in your project. You can do this with `nix run .#project-generator game src` which will watch your
project for changes.
