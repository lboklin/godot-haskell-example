# Change the path to the resulting shared library as needed
SPATH=$(readlink result) nix build \
  || echo "Build failed" \
  && nix store delete $SPATH &>/dev/null \
  || echo "Nothing to garbage collect" \
  && cp -f result/lib/ghc-8.10.7/libgodot-haskell-project.so ./game/lib/
